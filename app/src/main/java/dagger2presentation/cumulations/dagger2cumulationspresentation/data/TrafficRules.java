package dagger2presentation.cumulations.dagger2cumulationspresentation.data;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger2presentation.cumulations.dagger2cumulationspresentation.data.dagger.AppComponent;
import dagger2presentation.cumulations.dagger2cumulationspresentation.data.dagger.DaggerAppComponent;

import static android.R.attr.country;

/**
 * Created by praveenkumar on 26/05/17.
 */

public class TrafficRules {

    HashMap<String, Integer> speedAgeMap;

    public TrafficRules(HashMap map){

        speedAgeMap=map;
    }

    public int getAgelimit(String country) {

        return speedAgeMap.get(country+"-Age");
    }

    public int getPermittedSpeed(String country) {
        return speedAgeMap.get(country+"-Speed");
    }
}
