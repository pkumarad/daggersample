package dagger2presentation.cumulations.dagger2cumulationspresentation.data;

import android.app.Application;

import dagger2presentation.cumulations.dagger2cumulationspresentation.data.dagger.AppComponent;
import dagger2presentation.cumulations.dagger2cumulationspresentation.data.dagger.DaggerAppComponent;
import dagger2presentation.cumulations.dagger2cumulationspresentation.data.dagger.DaggerModule;

/**
 * Created by praveenkumar on 26/05/17.
 */

public class MainApplication extends Application{

    AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        init(this);

    }

     public  void   init(Application application){
      component=  DaggerAppComponent.builder().daggerModule(new DaggerModule(this)).build();
    }

    public AppComponent getAppComponent(){
        return component;
    }
}
