package dagger2presentation.cumulations.dagger2cumulationspresentation.data;

import javax.inject.Inject;

/**
 * Created by praveenkumar on 26/05/17.
 */

public class Car {

    TrafficRules rules;

    @Inject
    public Car(TrafficRules rules){
        this.rules=rules;
    }

    int getPermittedSpeed(String s){
        return rules.getPermittedSpeed(s);
    }

    int getAgeLimit(String s){
        return rules.getAgelimit(s);
    }

}
