package dagger2presentation.cumulations.dagger2cumulationspresentation.data.dagger;

import android.app.Application;

import java.util.HashMap;
import java.util.Map;

import dagger.Module;
import dagger.Provides;
import dagger2presentation.cumulations.dagger2cumulationspresentation.data.Car;
import dagger2presentation.cumulations.dagger2cumulationspresentation.data.TrafficRules;

/**
 * Created by praveenkumar on 26/05/17.
 */

@Module
public class DaggerModule {

    Application application;
    public  DaggerModule(Application app){
        application=app;
    }
    @Provides
   TrafficRules provideTrafficRules(){
        HashMap<String, Integer> stringIntegerHashMap = provideMap();
        return new TrafficRules(stringIntegerHashMap);
    }

    @Provides
    Application provideApplication() {
        return application;
    }

/*

    @Provides
    Car provideCar(){
       return new Car(provideTrafficRules());
    }
*/

    @Provides
    HashMap<String, Integer> provideMap(){

        HashMap<String, Integer> stringIntegerHashMap = new HashMap<>();

        stringIntegerHashMap.put("USA-Age",16);
        stringIntegerHashMap.put("India-Age",18);
        stringIntegerHashMap.put("India-Speed",160);
        stringIntegerHashMap.put("USA-Speed",140);

        return stringIntegerHashMap;
    }
}
