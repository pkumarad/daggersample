package dagger2presentation.cumulations.dagger2cumulationspresentation.data.dagger;

import android.app.Application;

import dagger.Component;
import dagger2presentation.cumulations.dagger2cumulationspresentation.data.MainActivity;
import dagger2presentation.cumulations.dagger2cumulationspresentation.data.TrafficRules;

/**
 * Created by praveenkumar on 26/05/17.
 */

@Component(modules = {DaggerModule.class})
public interface AppComponent  {

    void inject(MainActivity activity);

}
