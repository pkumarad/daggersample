package dagger2presentation.cumulations.dagger2cumulationspresentation.data;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.TextView;

import javax.inject.Inject;

import dagger2presentation.cumulations.dagger2cumulationspresentation.R;

/**
 * Created by praveenkumar on 26/05/17.
 */

public class MainActivity extends Activity {

    @Inject
    TrafficRules rules;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        TextView textView=(TextView) findViewById(R.id.textfield);

        MainApplication application=(MainApplication) getApplication();
        application.getAppComponent().inject(this);

        Car c=new Car(rules);
        Log.d("Cumulations", "Permitted age is "+ c.getAgeLimit("USA"));
        Log.d("Cumulations", "Permitted speed is "+ c.getPermittedSpeed("India"));

        textView.setText("Permitted Age is= "+c.getAgeLimit("USA") +"\n" +" Permitted Speed is= "+ c.getPermittedSpeed("USA"));



    }
}
